import Vue from 'vue'
import VueRouter from 'vue-router'
import StartScreen from "@/components/StartScreen";
import Play from "@/components/Play";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'StartScreen',
    component: StartScreen
  },
  {
    path: `/play/:playerName`,
    name: 'Play',
    component: Play
}

]

const router = new VueRouter({
  routes
})

export default router
