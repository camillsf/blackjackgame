

export const CardAPI = {
    fetchCard(deckId) {
        return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=1`)
            .then(response => response.json())
    },
    fetchTwoCards(deckId) {
        return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=2`)
            .then(response => response.json())
    }
}
