import {DeckAPI} from "./components/Deck/DeckAPI";
import {CardAPI} from "@/components/Card/CardAPI";


export const store = {
    state: {
        deck : {
                success: '',
                deck_id: '',
                shuffled: '',
                remaining: ''
        },
        card: {
            image: '',
            value: '',
            suit: '',
            code: ''
        },
        cards: [],
        cardBackside: 'https://i.pinimg.com/originals/62/ea/00/62ea0046d9b332d23393a714b160fa58.jpg'
    },

    getDeck() {
        return DeckAPI.fetchDeck().then(deck => {
            this.setDeck(deck)
        })
    },
    setDeck(deck = {}){
        this.state.deck = deck
    },
    getCard(deckId){
        return CardAPI.fetchCard(deckId).then(card => {
            this.setCard(card)
        })
    },
    setCard(card = {}){
        this.state.card =card.cards[0]
    },
    getTwoCards(deckId){
        return CardAPI.fetchTwoCards(deckId).then(cards => {
            this.setCards(cards)
        })
    },
    setCards(twoCards = []){
        this.state.cards = twoCards.cards
    }

}